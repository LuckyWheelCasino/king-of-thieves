# King of Thieves game #

### Main menu: ###

* the main menu is an entry point to the game
* the main menu has a play button that redirects a user to the game scene

### Game scene: ###

* the game scene represents a maze with square blocks
* the left-bottom cell is always free
* the hero spawns at the center of the left-bottom cell
* the hero collides with floors and walls
* the hero runs in the right direction by default
* a tap will force the hero to jump; a tap is possible only if the hero has at least one contact with a floor or a wall
* the hero stops running when they push against a wall
* if the hero slides on a wall a tap will force the hero not only to jump but to change a running direction as well
* each empty cell spawns a coin; there is a random spawn time after being collected
* the hero collects coins by touching them
* the game scene has a counter for collected coins
* the game scene has a countdown timer for collecting; when time runs out the game is over

### Approaches ###

* GameBootstraper in the Initial scene, for correct initialization
* Dependency Injection - using ProjectDependancyContainer for project and scene container (BaseContainer)
* Services registered in dependency container (ISettingsService, IAssetProvider, IPersistantProgressService...)
* Factory - creating gameplay features, etc
* Load resources from asset bundles by using Addresables  (IAssetProvider)
* GameStateMachine for controlling game flow 
* Using ScriptableObjects to control game balance, download in by using ISettingsService.
* Using interfaces:  to make changes easier if need to change on a different approach. For Example(ISaveLoadDataService is used for saving data on disk, in this case, we use PlayerPref for this purpose, but if we want to change saving to txt. We create a class inherited from ISaveLoadDataService, and add logic to saving txt. Register it (BootstraState -> RegisterServices), instead of PlayerPrefSaveLoadDataService)
* PlayerStateMachine for the control player
* MVC for Player: PlayerData, PlayerView, PlayerInputController, PlayerEnvironmentController
* Using async-await for async loading resources (Task could be heavy, instead better to use UniTask)

### Game mechanics ###

#Start from Initial scene

![Scheme](Instractions/1.png)

#For test 3 levels

![Scheme](Instractions/2.png)

#Input and UI in game scene

![Scheme](Instractions/3.png)

#Level configs: quantity coins achieve to win level, time to get all coins

![Scheme](Instractions/4.png)
