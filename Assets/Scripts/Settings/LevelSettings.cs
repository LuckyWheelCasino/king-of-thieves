﻿using UnityEngine;

namespace Settings.Level
{
  [CreateAssetMenu(fileName = "LevelSettings", menuName = "Settings/LevelSettings")]
  public class LevelSettings : ScriptableObject
  {
    public LevelData[] LevelsData;
  }
}