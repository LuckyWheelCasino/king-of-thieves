﻿using UnityEngine;

namespace Settings.Player
{
  [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Settings/PlayerSettings")]
  public class PlayerSettings : ScriptableObject
  {
    public float MaxSpeed = 4f;
    public float JumpForce = 600f;
    public float ClimbJumpForceUp = 500f;    
    public float ClimbJumpForceSide = 500f;

    public float DownSpeed = 2f;
  }
}