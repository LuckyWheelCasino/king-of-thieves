﻿using System;

namespace Settings.Level
{
  [Serializable]
  public class LevelData
  {
    public int Id;
    public int CoinsToWin = 4;
    public int LevelTimeDuration = 30;
  }
}