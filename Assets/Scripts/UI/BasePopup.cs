﻿using UnityEngine;

namespace UI
{
  public abstract class BasePopup : MonoBehaviour
  {
    public abstract void Show();
    public abstract void Hide();
  }
}