﻿using UnityEngine.Events;

namespace UI.Menu
{
  public class FinishContentPopup : BasePopup
  {
    public UnityEvent ShowFinishContentPopupEvent;
    public UnityEvent HideFinishContentPopupEvent;

    public override void Show()
    {
      ShowFinishContentPopupEvent?.Invoke();
    }

    public override void Hide()
    {
      HideFinishContentPopupEvent?.Invoke();
    }
  }
}