using Core.Data;
using Core.Services.PersistentProgress;
using UnityEngine;

namespace Core.Services.SaveLoad
{
  public class SaveLoadService : ISaveLoadService
  {
    private const string ProgressKey = "Progress";
    
    private readonly IPersistentProgressService _progressService;

    public SaveLoadService(IPersistentProgressService progressService)
    {
      _progressService = progressService;
    }

    public void SaveProgress()
    {
      var json = JsonUtility.ToJson(_progressService.Progress);
      PlayerPrefs.SetString(ProgressKey, json);
    }

    public PlayerProgress LoadProgress()
    {
      var json = PlayerPrefs.GetString(ProgressKey);
      return JsonUtility.FromJson<PlayerProgress>(json);
    }
  }
}