﻿using System;
using System.Collections;
using UnityEngine;

namespace Level.Service
{
  public class LocalTimerService : BaseTimerService
  {
    public override void StartTimer(float delay, Action onFinished)
    {
        StartCoroutine(StartTimerCoroutine(delay, onFinished));
    }

    private IEnumerator StartTimerCoroutine(float delay, Action onFinished)
    {
      yield return new WaitForSeconds(delay);
      onFinished?.Invoke();
    }
    
    public override void StartSecondsUpdateTimer(float delay, Action onFinished, Action onTick = null)
    {
      StartCoroutine(StartSecondUpdateTimerCoroutine(delay, onFinished, onTick));
    }
    
    private IEnumerator StartSecondUpdateTimerCoroutine(float delay, Action onFinished, Action onTick)
    {
      for (int i = 0; i < delay; i++)
      {
        yield return new WaitForSeconds(1);
        onTick?.Invoke();
      }

      onFinished?.Invoke();
    }
  }
}