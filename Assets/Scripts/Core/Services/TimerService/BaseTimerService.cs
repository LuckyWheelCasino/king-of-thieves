﻿using System;
using UnityEngine;

namespace Level.Service
{
  public abstract class BaseTimerService : MonoBehaviour
  {
    public abstract void StartTimer(float delay, Action onFinished);
    public abstract void StartSecondsUpdateTimer(float delay, Action onFinished, Action onTick = null);
  }
}