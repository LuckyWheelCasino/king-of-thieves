﻿using System.Threading.Tasks;
using Settings.Level;
using Settings.Player;

namespace Core.Services.Settings
{
  public interface ISettingService : IService
  {
    Task Load();
    PlayerSettings GetPlayerSettings();
    LevelSettings GetLevelSettings();
  }
}