namespace Core.Services
{
  public class ProjectDependencyContainer
  {
    private static ProjectDependencyContainer _instance;
    public static ProjectDependencyContainer Container => _instance ?? (_instance = new ProjectDependencyContainer());

    public void RegisterSingle<TService>(TService implementation) where TService : IService =>
      Implementation<TService>.ServiceInstance = implementation;

    public TService Single<TService>() where TService : IService =>
      Implementation<TService>.ServiceInstance;

    private class Implementation<TService> where TService : IService
    {
      public static TService ServiceInstance;
    }
  }
}