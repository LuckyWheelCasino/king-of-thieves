﻿using System.Threading.Tasks;
using Core.Infrastructure.AssetManagement;
using Settings.Level;
using Settings.Player;

namespace Core.Services.Settings
{
  public class SettingService : ISettingService
  {
    private const string PlayerSettingsPath = "Settings/PlayerSettings";
    private const string LevelSettingsPath = "Settings/LevelSettings";

    private PlayerSettings _playerSettings;
    private LevelSettings _levelSettings;
    
    private readonly IAssetProvider _assetProvider;

    public SettingService(IAssetProvider assetProvider)
    {
      _assetProvider = assetProvider;
    }

    public async Task Load()
    {
      _playerSettings = await _assetProvider.Load<PlayerSettings>(PlayerSettingsPath);
      _levelSettings = await _assetProvider.Load<LevelSettings>(LevelSettingsPath);
    }

    public PlayerSettings GetPlayerSettings()
    {
      return _playerSettings;
    }

    public LevelSettings GetLevelSettings()
    {
      return _levelSettings;
    }
  }
}