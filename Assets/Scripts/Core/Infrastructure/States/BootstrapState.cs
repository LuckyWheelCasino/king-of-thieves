﻿using Core.Infrastructure.AssetManagement;
using Core.Services;
using Core.Services.PersistentProgress;
using Core.Services.SaveLoad;
using Core.Services.Settings;


namespace Core.Infrastructure.States
{
  public class BootstrapState : IState
  {
    private const string Initial = "Initial";
    
    private readonly GameStateMachine _stateMachine;
    private readonly SceneLoaderService _sceneLoaderService;
    private readonly ProjectDependencyContainer _services;

    public BootstrapState(GameStateMachine stateMachine, SceneLoaderService sceneLoaderService, ProjectDependencyContainer services)
    {
      _stateMachine = stateMachine;
      _sceneLoaderService = sceneLoaderService;
      _services = services;
      
      RegisterServices();
    }

    public void Enter()
    {
      Warmup();
    }

    public void Exit()
    {
    }

    private async void Warmup()
    {
      var settingService = _services.Single<ISettingService>();
      await settingService.Load();
      _sceneLoaderService.Load(Initial, onLoaded: EnterLoadLevel);
    }

    private void RegisterServices()
    {
      _services.RegisterSingle<IGameStateMachine>(_stateMachine);

      AssetProvider assetProvider = new AssetProvider();
      _services.RegisterSingle<IAssetProvider>(assetProvider);
      assetProvider.Init();
      
      ISettingService settingService = new SettingService(_services.Single<IAssetProvider>());
      _services.RegisterSingle<ISettingService>(settingService);

      _services.RegisterSingle<IPersistentProgressService>(new PersistentProgressService());
      _services.RegisterSingle<ISaveLoadService>(new SaveLoadService(_services.Single<IPersistentProgressService>()));
    }

    private void EnterLoadLevel()
    {
      _stateMachine.Enter<LoadProgressState>();
    }
  }
}