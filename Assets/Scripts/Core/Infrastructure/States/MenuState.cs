﻿using Core.Infrastructure.AssetManagement;
using Core.Services.PersistentProgress;
using Core.Services.Settings;
using Level.Infrastructure.Container;
using Settings.Level;
using UnityEngine;

namespace Core.Infrastructure.States
{
  public class MenuState : IState
  {
    private readonly GameStateMachine _stateMachine;

    private MenuDependencyContainer _container = default;
    private IPersistentProgressService _persistentProgressService;
    private ISettingService _settingService;
    private IAssetProvider _assetProvider;

    public MenuState(GameStateMachine gameStateMachine, IPersistentProgressService persistentProgressService, ISettingService settingService, IAssetProvider assetProvider)
    {
      _persistentProgressService = persistentProgressService;
      _stateMachine = gameStateMachine;
      _settingService = settingService;
      _assetProvider = assetProvider;
    }
    
    public void Enter()
    {
      _container = GameObject.FindObjectOfType<BaseContainer>() as MenuDependencyContainer;
      int currentLevel = _persistentProgressService.Progress.CurrentLevel;

      LevelSettings settings = _settingService.GetLevelSettings();
      int levelsAmount = settings.LevelsData.Length;
      
      if (currentLevel > levelsAmount)
      {
        currentLevel = -1;
      }

      _container?.UIManager?.Init(_container.BaseTimerService, _assetProvider, _persistentProgressService.Progress.CoinsAmount, currentLevel, PlayLevel);
    }

    private void PlayLevel()
    {
      _stateMachine.Enter<LoadGameLevelState>();
    }

    public void Exit()
    {
    }
  }
}