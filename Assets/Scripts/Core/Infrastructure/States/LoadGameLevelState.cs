﻿using Core.Logic;

namespace Core.Infrastructure.States
{
  public class LoadGameLevelState : IState
  {
    private const string _gameLevel = "GameLevel";

    private readonly GameStateMachine _stateMachine;
    private readonly SceneLoaderService _sceneLoaderService;
    private readonly LoadingCurtain _loadingCurtain;

    public LoadGameLevelState(GameStateMachine gameStateMachine, SceneLoaderService sceneLoaderService, LoadingCurtain loadingCurtain)
    {
      _stateMachine = gameStateMachine;
      _sceneLoaderService = sceneLoaderService;
      _loadingCurtain = loadingCurtain;
    }
    
    public void Enter()
    {
      _loadingCurtain.Show();
      _sceneLoaderService.Load(_gameLevel, OnLoaded);
    }

    private void OnLoaded()
    {
      _stateMachine.Enter<GameLevelState>();
    }
    
    public void Exit()
    {
      _loadingCurtain.Hide();
    }
  }
}