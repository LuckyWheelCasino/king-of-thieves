﻿using System;
using System.Collections.Generic;
using Core.Infrastructure.AssetManagement;
using Core.Logic;
using Core.Services;
using Core.Services.PersistentProgress;
using Core.Services.SaveLoad;
using Core.Services.Settings;

namespace Core.Infrastructure.States
{
  public class GameStateMachine : IGameStateMachine
  {
    private Dictionary<Type, IExitableState> _states;
    private IExitableState _activeState;

    public GameStateMachine(SceneLoaderService sceneLoaderService, LoadingCurtain loadingCurtain, ProjectDependencyContainer services)
    {
      _states = new Dictionary<Type, IExitableState>
      {
        [typeof(BootstrapState)] = new BootstrapState(this, sceneLoaderService, services),
        [typeof(LoadProgressState)] = new LoadProgressState(this, services.Single<IPersistentProgressService>(), services.Single<ISaveLoadService>()),
        [typeof(LoadMenuState)] = new LoadMenuState(this, sceneLoaderService, loadingCurtain),
        [typeof(MenuState)] = new MenuState(this, services.Single<IPersistentProgressService>(), services.Single<ISettingService>(), services.Single<IAssetProvider>()),
        [typeof(LoadGameLevelState)] = new LoadGameLevelState(this, sceneLoaderService, loadingCurtain),
        [typeof(GameLevelState)] = new GameLevelState(this, services.Single<IPersistentProgressService>(), services.Single<ISettingService>(), services.Single<ISaveLoadService>()),
      };
    }
    
    public void Enter<TState>() where TState : class, IState
    {
      IState state = ChangeState<TState>();
      state.Enter();
    }

    public void Enter<TState, TPayload>(TPayload payload) where TState : class, IPayloadedState<TPayload>
    {
      TState state = ChangeState<TState>();
      state.Enter(payload);
    }

    private TState ChangeState<TState>() where TState : class, IExitableState
    {
      _activeState?.Exit();
      
      TState state = GetState<TState>();
      _activeState = state;
      
      return state;
    }

    private TState GetState<TState>() where TState : class, IExitableState
    {
      return _states[typeof(TState)] as TState;
    }
  }
}