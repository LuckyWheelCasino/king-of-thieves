﻿using Core.Services.PersistentProgress;
using Core.Logic;

namespace Core.Infrastructure.States
{
  public class LoadMenuState : IState
  {
    private const string _menuStr = "Menu";
    
    private readonly GameStateMachine _stateMachine;
    private readonly SceneLoaderService _sceneLoaderService;
    private readonly LoadingCurtain _loadingCurtain;

    public LoadMenuState(GameStateMachine gameStateMachine, SceneLoaderService sceneLoaderService, LoadingCurtain loadingCurtain)
    {
      _stateMachine = gameStateMachine;
      _sceneLoaderService = sceneLoaderService;
      _loadingCurtain = loadingCurtain;
    }

    public void Enter()
    {
      _loadingCurtain.Show();
      _sceneLoaderService.Load(_menuStr, OnLoaded);
    }
    
    public void Exit()
    {
      _loadingCurtain.Hide();
    }

    private void OnLoaded()
    {
      _stateMachine.Enter<MenuState>();
    }
  }
}