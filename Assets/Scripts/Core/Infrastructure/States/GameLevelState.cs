﻿using System.Linq;
using Core.Infrastructure.AssetManagement;
using Core.Services;
using Core.Services.PersistentProgress;
using Core.Services.SaveLoad;
using Core.Services.Settings;
using Level.Infrastructure.Container;
using Settings.Level;
using UnityEngine;

namespace Core.Infrastructure.States
{
  public class GameLevelState : IState
  {
    private readonly GameStateMachine _stateMachine;

    private int _coinsToWinLeft = 3;
    private int _secondLeft = 3;

    private GameLevelDependencyContainer _gameLevelDependencyContainer = default;
    private IPersistentProgressService _persistentProgressService;
    private ISettingService _settingsService;
    private ISaveLoadService _saveLoadService;

    public GameLevelState(GameStateMachine gameStateMachine, IPersistentProgressService persistentProgressService,
      ISettingService settingService, ISaveLoadService saveLoadService)
    {
      _persistentProgressService = persistentProgressService;
      _settingsService = settingService;
      _stateMachine = gameStateMachine;
      _saveLoadService = saveLoadService;
    }

    public void Enter()
    {
      _gameLevelDependencyContainer = GameObject.FindObjectOfType<BaseContainer>() as GameLevelDependencyContainer;

      var progressService = ProjectDependencyContainer.Container.Single<IPersistentProgressService>();
      var assetProvider = ProjectDependencyContainer.Container.Single<IAssetProvider>();
      var settingsService = ProjectDependencyContainer.Container.Single<ISettingService>();

      CreateLevel(progressService, assetProvider, settingsService);
    }

    private async void CreateLevel(IPersistentProgressService persistentProgressService, IAssetProvider assetProvider,
      ISettingService settingsService)
    {
      int levelNum = persistentProgressService.Progress.CurrentLevel;
      int coinsAmount = persistentProgressService.Progress.CoinsAmount;

      LevelSettings levelSettings = _settingsService.GetLevelSettings();
      LevelData data = levelSettings.LevelsData.First(x => x.Id == levelNum);
      _coinsToWinLeft = data.CoinsToWin;
      _secondLeft = data.LevelTimeDuration;
      
      _gameLevelDependencyContainer.LevelFactory.Init(_gameLevelDependencyContainer, assetProvider, settingsService);
      _gameLevelDependencyContainer.UIManager.Init(coinsAmount, _secondLeft, Lose);
      _gameLevelDependencyContainer.TimerService.StartSecondsUpdateTimer(_secondLeft, Lose, _gameLevelDependencyContainer.UIManager.UpdateTimer);

      Level.Infrastructure.Level level = await _gameLevelDependencyContainer.LevelFactory.CreateLevel(levelNum);

      GameObject player = await _gameLevelDependencyContainer.LevelFactory.CreatePlayer(level.PlayerStartPosition);
      _gameLevelDependencyContainer.Player = player.transform;

      foreach (Transform position in level.CoinSpawnerPositions)
      {
        var spawner = await _gameLevelDependencyContainer.LevelFactory.CreatCoinSpawner(position);
        spawner.CollectCoinEvent += UpdateCoinsCounter;
      }
    }

    private void UpdateCoinsCounter()
    {
      _coinsToWinLeft--;
      _persistentProgressService.Progress.CoinsAmount++;

      if (_coinsToWinLeft <= 0)
      {
        Win();
      }
    }

    private void Win()
    {
      _persistentProgressService.Progress.CurrentLevel++;
      _saveLoadService.SaveProgress();

      _stateMachine.Enter<LoadMenuState>();
    }

    private void Lose()
    {
      _stateMachine.Enter<LoadMenuState>();
    }

    public void Exit()
    {
    }
  }
}