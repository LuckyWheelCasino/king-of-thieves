﻿using Core.Infrastructure.States;
using Core.Logic;
using Core.Services;
using UnityEngine;

namespace Core.Infrastructure
{
  public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
  {
    public LoadingCurtain CurtainPrefab;

    private void Awake()
    {
      StartGameStateMachine();

      DontDestroyOnLoad(this);
    }

    private void StartGameStateMachine()
    {
      var stateMachine = new GameStateMachine(new SceneLoaderService(this), Instantiate(CurtainPrefab),
        ProjectDependencyContainer.Container);
      
      stateMachine.Enter<BootstrapState>();
    }
  }
}