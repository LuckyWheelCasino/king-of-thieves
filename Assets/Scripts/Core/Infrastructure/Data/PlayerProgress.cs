﻿namespace Core.Data
{
  public class PlayerProgress
  {
    public int CurrentLevel = 1;
    public int CoinsAmount = 0;
  }
}