using System.Threading.Tasks;
using Core.Services;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Core.Infrastructure.AssetManagement
{
  public interface IAssetProvider : IService
  {
    void Init();
    Task<T> Load<T>(string address) where T : class;
    Task<GameObject> Instantiate(string address, Transform under);
    Task<GameObject> Instantiate(string address);
    void Cleanup();
  }
}