namespace Core.Infrastructure.AssetManagement
{
  public static class AssetAddress
  {
    public const string Player = "Player";
    public const string CoinSpawner = "CoinSpawner";
    public const string Level = "Level";
    public const string ContentFinishWindow = "UI/ContentFinishWindow";
  }
}