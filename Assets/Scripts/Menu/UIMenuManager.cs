using System;
using Core.Infrastructure.AssetManagement;
using Level.Service;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
  public class UIMenuManager : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI _coinCounter;
    [SerializeField] private TextMeshProUGUI _textButton;
    [SerializeField] private Button _playLevelButton;
    [SerializeField] private Transform _content;
    
    private BaseTimerService _timerService;
    private IAssetProvider _assetProvider;
    private bool _contentFinishPopupShowed;

    public event Action PlayLevelButtonClickEvent;
    
    private int _coinsAmount = 0;

    public void Init(BaseTimerService timerService, IAssetProvider assetProvider, int coinsAmount, int level, Action playLevel)
    {
      _timerService = timerService;
      _assetProvider = assetProvider;
      
      if (level == -1)
      {
        UpdateCoinsCounter(coinsAmount);
        _textButton.text = "";
        _playLevelButton.onClick.AddListener(ShowFinishContentPopup);
        
        return;
      }

      PlayLevelButtonClickEvent += playLevel;
      UpdateCoinsCounter(coinsAmount);
      UpdateLevelNumber(level);
      _playLevelButton.onClick.AddListener(()=> PlayLevelButtonClickEvent?.Invoke());
    }

    public void UpdateCoinsCounter(int amount)
    {
      _coinsAmount = amount;
      _coinCounter.text = $"Coins: {_coinsAmount}";
    }
    
    public void UpdateLevelNumber(int level)
    {
      _textButton.text = $"Play level {level}";
    }

    private async void ShowFinishContentPopup()
    {
      if (_contentFinishPopupShowed)
      {
        return;
      }

      void DestroyPopup(GameObject popup)
      {
        Destroy(popup);
        _contentFinishPopupShowed = false;
      }
      
      var popup = await _assetProvider.Instantiate(AssetAddress.ContentFinishWindow, _content);
      popup.GetComponent<BasePopup>().Show();
      _timerService.StartTimer(3f, ()=> DestroyPopup(popup));
    }
  }
}