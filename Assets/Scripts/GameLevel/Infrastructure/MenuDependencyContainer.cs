﻿using Level.Service;
using Menu;
using UnityEngine;

namespace Level.Infrastructure.Container
{
  public class MenuDependencyContainer : BaseContainer
  {
    [SerializeField] private UIMenuManager _uiManager;
    [SerializeField] private BaseTimerService _timerService;

    
    public UIMenuManager UIManager => _uiManager;
    public BaseTimerService BaseTimerService => _timerService;
  }
}