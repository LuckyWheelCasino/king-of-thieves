﻿using System.Threading.Tasks;
using Core.Infrastructure.AssetManagement;
using Core.Services.Settings;
using Level.GameFeature;
using Level.Infrastructure.Container;
using UnityEngine;

namespace Level.Infrastructure
{
  public abstract class BaseLevelFactory : MonoBehaviour
  {
    public abstract void Init(GameLevelDependencyContainer container, IAssetProvider assetProvider, ISettingService settingService);
    public abstract Task<Level> CreateLevel(int levelId);
    public abstract Task<GameObject> CreatePlayer(Transform startPosition);
    public abstract Task<SimpleCoinSpawner> CreatCoinSpawner(Transform position);
  }
}