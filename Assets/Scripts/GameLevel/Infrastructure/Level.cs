using UnityEngine;

namespace Level.Infrastructure
{
  public class Level : MonoBehaviour
  {
    [SerializeField] private int _id;
    [SerializeField] private Transform _playerStartPosition;
    [SerializeField] private Transform[] _coinSpawnerPositions;
  
    public int Id => _id;
    public Transform  PlayerStartPosition=> _playerStartPosition;
    public Transform[] CoinSpawnerPositions => _coinSpawnerPositions;
  }
}