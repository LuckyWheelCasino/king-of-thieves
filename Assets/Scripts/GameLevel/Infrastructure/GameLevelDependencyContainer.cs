﻿using Level.Service;
using Level.UI;
using UnityEngine;

namespace Level.Infrastructure.Container
{
  public class GameLevelDependencyContainer : BaseContainer
  {
    [SerializeField] private BaseLevelFactory _factory;
    [SerializeField] private BaseTimerService _timerService;
    [SerializeField] private UIManager _uiManager;
  
    public BaseTimerService TimerService => _timerService;
    public BaseLevelFactory LevelFactory => _factory;
    public UIManager UIManager => _uiManager;
  
    [HideInInspector]
    public Transform Player;
  }
}