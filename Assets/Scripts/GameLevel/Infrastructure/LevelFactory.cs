﻿using System.Threading.Tasks;
using Core.Infrastructure.AssetManagement;
using Core.Services.Settings;
using Level.GameFeature;
using Level.Infrastructure.Container;
using Settings.Player;
using UnityEngine;

namespace Level.Infrastructure
{
  public class LevelFactory : BaseLevelFactory
  {
    private GameLevelDependencyContainer _container;
    private IAssetProvider _assetProvider;
    private ISettingService _settingService;

    public override void Init(GameLevelDependencyContainer container, IAssetProvider assetProvider, ISettingService settingService)
    {
      _container = container;
      _assetProvider = assetProvider;
      _settingService = settingService;
    }

    public override async Task<Level> CreateLevel(int levelId)
    {
      var level =  await _assetProvider.Instantiate($"{AssetAddress.Level}_{levelId}");
     
      return level.GetComponent<Level>();
    }

    public override async Task<GameObject> CreatePlayer(Transform startPosition)
    {
      var player =  await _assetProvider.Instantiate(AssetAddress.Player);
      player.transform.position = startPosition.position;
      PlayerSettings settings = _settingService.GetPlayerSettings();
      player.GetComponent<Player.Player>().Init(settings);
     
      return player;
    }

    public override async Task<SimpleCoinSpawner> CreatCoinSpawner(Transform targetPosition)
    {
      GameObject spawnerGameObject =  await _assetProvider.Instantiate(AssetAddress.CoinSpawner);
      SimpleCoinSpawner spawner = spawnerGameObject.GetComponent<SimpleCoinSpawner>();
      spawner.transform.position = targetPosition.position;
      spawner.Init(_container.Player, _container.TimerService);
      spawner.CollectCoinEvent += _container.UIManager.IncreaseCoinCounter;

      return spawner;
    }
  }
}