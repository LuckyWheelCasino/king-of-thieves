﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Level.UI
{
  public class UIManager : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI _coinCounter;
    [SerializeField] private TextMeshProUGUI _timer;
    [SerializeField] private Button _backMenuButton;
    
    private int _coinsAmount = 0;
    private int _secondsLeft = 0;

    public void Init(int coinsAmount, int secondsLeft, Action backMenu)
    {
      _secondsLeft = secondsLeft;
      _coinsAmount = coinsAmount;
      
      _coinCounter.text = _coinsAmount.ToString();
      _timer.text = _secondsLeft.ToString();
      
      _backMenuButton.onClick.AddListener(()=> backMenu?.Invoke());
    }

    public void IncreaseCoinCounter()
    {
      _coinsAmount++;
      _coinCounter.text = _coinsAmount.ToString();
    }

    public void UpdateTimer()
    {
      _secondsLeft--;
      _timer.text = _secondsLeft.ToString();
    }
  } 
}