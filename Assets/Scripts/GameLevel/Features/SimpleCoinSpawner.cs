using System;
using Level.Service;
using UnityEngine;
using UnityEngine.Events;

namespace Level.GameFeature
{
  public class SimpleCoinSpawner : MonoBehaviour
  {
    [SerializeField] private float _collectDistance = 1f;
    [SerializeField] private bool _loop = true;
    [SerializeField] private float _delay = 5f;

    private BaseTimerService _timerService;
    private Transform _player;
    private bool _isReady;

    public UnityEvent HideCoinEvent;
    public UnityEvent ShowCoinEvent;
    
    public event Action CollectCoinEvent;

    public void Init(Transform player, BaseTimerService timerService)
    {
      _player = player;
      _timerService = timerService;

      SetReadyToCollect();
    }

    private void Update()
    {
      if (!_isReady)
      {
        return;
      }

      if (Vector2.Distance(_player.transform.position, transform.position) < _collectDistance)
      {
        TryCollectCoin();
      }
    }

    private void SetReadyToCollect()
    {
      ShowCoinEvent?.Invoke();
      _isReady = true;
    }

    private void TryCollectCoin()
    {
      _isReady = false;
      HideCoinEvent?.Invoke();
      CollectCoinEvent?.Invoke();

      if (_loop)
      {
        _timerService.StartTimer(_delay, SetReadyToCollect);
      }
    }
  }
}
