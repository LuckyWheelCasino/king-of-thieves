﻿using Player.MVC.Controller;
using Player.MVC.Data;
using Player.MVC.View;
using Player.StateMachine;
using Player.StateMachine.State;
using Settings.Player;
using UnityEngine;

namespace Player
{
  public class Player : MonoBehaviour
  {
    [SerializeField] private PlayerInputController _playerInputController;
    [SerializeField] private PlayerEnvironmentController _playerEnvironmentController;
    [SerializeField] private PlayerView _playerView;

    [SerializeField] private MovementState _movementState;
    [SerializeField] private ClimbingState _climbingState;
    [SerializeField] private StopState _stopState;
    
    private PlayerStateMachine _playerStateMachine;
    
    public void Init(PlayerSettings playerSettings)
    {
      PlayerData data = new PlayerData();
      data.PlayerSettings = playerSettings;
      
      _playerInputController.Init(data);
      _playerEnvironmentController.Init(data);
      _playerView.Init(data);

      _playerStateMachine = new PlayerStateMachine(_movementState, _climbingState, _stopState);

      _movementState.Init(_playerStateMachine, data);
      _climbingState.Init(_playerStateMachine, data);
      _stopState.Init(_playerStateMachine, data);
      
      _playerStateMachine.Enter<MovementState>();
    }

    public void Update()
    {
      _playerStateMachine?.TickActiveState();
    }
  }
}