﻿using UnityEngine;

namespace Player.StateMachine.State
{
  public class ClimbingState : BaseState
  {
    [SerializeField] private Rigidbody2D _rigidbody;

    public override void Enter()
    {
      base.Enter();
      _rigidbody.velocity = new Vector2(0, 0);
    }
    
    public override void Tick()
    {
      if(!IsReady)
        return;

      TryMoveDown();
      TryJump();
      TryRun();
      TryStop();
    }

    private void TryRun()
    {
      if (_data.Grounded && !_data.Wall && !_data.Jump)
      {
        _playerStateMachine.Enter<MovementState>();
      }
    }
    
    private void TryStop()
    {
      if (_data.Grounded && _data.Wall)
      {
        _playerStateMachine.Enter<StopState>();
      }
    }
    
    private void TryMoveDown()
    {
      if (!_data.Grounded && _data.Wall && !_data.Jump)
      {
        MoveDown();
      }
    }
    
    private void TryJump()
    {
      if (!_data.Grounded && _data.Wall && _data.Jump)
      {
        Flip();
        Jump();
      }
    }

    private void MoveDown()
    {
      _rigidbody.velocity = new Vector2(0, -_data.PlayerSettings.DownSpeed);
    }

    private void Jump()
    {
      _rigidbody.velocity = new Vector2(0, 0);
      
      _data.Grounded = false;
      _data.Wall = false;
      _data.Jump = false;

      float forceSide = _data.PlayerSettings.ClimbJumpForceSide;
      float forceUp = _data.PlayerSettings.ClimbJumpForceUp;
      forceSide *= _data.FacingRight ? 1f : -1f;
      
      _rigidbody.AddForce(new Vector2(forceSide, forceUp));
    }
  }
}