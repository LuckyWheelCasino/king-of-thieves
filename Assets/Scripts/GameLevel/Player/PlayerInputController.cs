using Player.MVC.Data;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Player.MVC.Controller
{
    public class PlayerInputController : MonoBehaviour
    {
        [SerializeField] private bool _infinityMovement = true;
        
        private const string _jumpStr = "Jump";
        private const string _horizontalStr = "Horizontal";
        
        private PlayerData _data;

        public void Init(PlayerData data)
        {
            _data = data;
        }

        private void Update()
        {
            if (!IsReady()) 
                return;

            if (!_data.Jump && (_data.Wall || _data.Grounded))
            {
                _data.Jump = CrossPlatformInputManager.GetButtonDown(_jumpStr);
            }
        }

        private void FixedUpdate()
        {
            if (!IsReady()) 
                return;
            
            float acceleration = CrossPlatformInputManager.GetAxis(_horizontalStr);

            if (_infinityMovement)
            {
                acceleration = UpdateAcceleration(acceleration);
            }

            _data.Move = acceleration;
        }

        private float UpdateAcceleration(float acceleration)
        {
            if (acceleration < 0)
            {
                acceleration = -1f;
            }
            else if (acceleration > 0)
            {
                acceleration = 1f;
            }
            else
            {
                if (_data.FacingRight)
                {
                    acceleration = 1f;
                }
                else
                {
                    acceleration = -1f;
                }
            }

            return acceleration;
        }

        private bool IsReady()
        {
            if (_data == null)
            {
                return false;
            }

            return true;
        }
    }
}
