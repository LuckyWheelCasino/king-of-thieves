﻿using Settings.Player;

namespace Player.MVC.Data
{
  public class PlayerData
  {
    public bool Grounded;
    public bool Wall;
    public float Move = 1f;
    public bool Jump;
    public bool FacingRight = true;

    public PlayerSettings PlayerSettings;
    
    public bool LockMovement;
  }
}