﻿using System;
using System.Collections.Generic;
using Player.StateMachine.State;

namespace Player.StateMachine
{
  public class PlayerStateMachine
  {
    private Dictionary<Type, IPlayerState> _states;
    private IPlayerState _activeState;

    public PlayerStateMachine(MovementState movementState, ClimbingState climbingState, StopState stopState)
    {
      _states = new Dictionary<Type, IPlayerState>
      {
        [typeof(MovementState)] = movementState,
        [typeof(ClimbingState)] = climbingState,
        [typeof(StopState)] = stopState,
      };
    }
    
    public void Enter<TState>() where TState : class, IPlayerState
    {
      IPlayerState state = ChangeState<TState>();
      state.Enter();
    }

    private TState ChangeState<TState>() where TState : class, IPlayerState
    {
      _activeState?.Exit();
      
      TState state = GetState<TState>();
      _activeState = state;
      
      return state;
    }

    private TState GetState<TState>() where TState : class, IPlayerState
    {
      return _states[typeof(TState)] as TState;
    }

    public void TickActiveState()
    {
      _activeState?.Tick();
    }
  }
}