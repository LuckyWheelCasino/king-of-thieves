﻿using Player.MVC.Data;
using UnityEngine;

namespace Player.MVC.Controller
{
  public class PlayerEnvironmentController : MonoBehaviour
  {
    private const float _groundedRadiusStr = .2f; 
    private const float _wallRadiusStr = .2f; 

    [SerializeField] private LayerMask _wallFlag;
    [SerializeField] private Transform[] _wallCheck; 
    [SerializeField] private LayerMask _groundFlag;
    [SerializeField] private Transform _groundCheck; 
        
    private PlayerData _data;

    public void Init(PlayerData data)
    {
      _data = data;
    }
    
    public void FixedUpdate()
    {
      if (!IsReady()) 
        return;
      
      DetectGround();
      DetectWall();
    }

    private void DetectGround()
    {
      _data.Grounded = false;

      Collider2D[] colliders = Physics2D.OverlapCircleAll(_groundCheck.position, _groundedRadiusStr, _groundFlag);

      for (int c = 0; c < colliders.Length; c++)
      {
        if (colliders[c].gameObject != gameObject)
        {
          _data.Grounded = true;
        }
      }
    }
    
    private void DetectWall()
    {
      _data.Wall = false;

      for (int i = 0; i < _wallCheck.Length; i++)
      {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(_wallCheck[i].position, _wallRadiusStr,_wallFlag);

        for (int c = 0; c < colliders.Length; c++)
        {
          if (colliders[c].gameObject != gameObject)
          {
            _data.Wall = true;
          }
        }
      }
    }
    
    private bool IsReady()
    {
      if (_data == null)
      {
        return false;
      }

      return true;
    }
  }
}