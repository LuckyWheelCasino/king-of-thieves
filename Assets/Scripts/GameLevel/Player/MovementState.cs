using UnityEngine;

namespace Player.StateMachine.State
{
  public class MovementState : BaseState
  {
    [SerializeField] private Rigidbody2D _rigidbody;

    public override void Tick()
    {
      if (!IsReady)
        return;

      Move();
      TryChangerDirection();
      TryJump();
      TryStop();
      TryClimb();
    }

    private void TryJump()
    {
      if (_data.Grounded && !_data.Wall && _data.Jump)
      {
        Jump();
      }
    }

    private void TryStop()
    {
      if (_data.Grounded && _data.Wall)
      {
        _playerStateMachine.Enter<StopState>();
      }
    }

    private void TryClimb()
    {
      if (!_data.Grounded && _data.Wall && !_data.Jump)
      {
        _playerStateMachine.Enter<ClimbingState>();
      }
    }

    private void TryChangerDirection()
    {
      if (_data.Move > 0 && !_data.FacingRight)
      {
        Flip();
      }
      else if (_data.Move < 0 && _data.FacingRight)
      {
        Flip();
      }
    }

    private void Move()
    {
      _rigidbody.velocity = new Vector2(_data.Move * _data.PlayerSettings.MaxSpeed, _rigidbody.velocity.y);
    }

    private void Jump()
    {
      _data.Grounded = false;
      _rigidbody.AddForce(new Vector2(0f, _data.PlayerSettings.JumpForce));
      _data.Jump = false;
    }
  }
}