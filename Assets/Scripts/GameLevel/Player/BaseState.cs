﻿using Player.MVC.Data;
using UnityEngine;

namespace Player.StateMachine.State
{
  public abstract class BaseState : MonoBehaviour, IPlayerState
  {
    protected PlayerData _data;
    protected PlayerStateMachine _playerStateMachine;
    
    private bool _isReady = false;
    
    public void Init(PlayerStateMachine playerStateMachine, PlayerData data)
    {
      _playerStateMachine = playerStateMachine;
      _data = data;
    }
    
    public virtual void Enter()
    {
      _isReady = true;
    }

    public abstract void Tick();

    public virtual void Exit()
    {
      _isReady = false;
    }
    
    protected void Flip()
    {
      _data.FacingRight = !_data.FacingRight;
      Vector3 scale = transform.localScale;
      scale.x *= -1;
      transform.localScale = scale;
    }

    protected bool IsReady => _isReady;
  }
}