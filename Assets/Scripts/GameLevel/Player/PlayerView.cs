﻿using System;
using Player.MVC.Data;
using UnityEngine;

namespace Player.MVC.View
{
  public class PlayerView : MonoBehaviour
  {
    [SerializeField] private Animator _currentAnimator; 
    [SerializeField] private Rigidbody2D _rigidbody;

    private PlayerData _data;
    
    private readonly int VSpeed = Animator.StringToHash("vSpeed");
    private readonly int Ground = Animator.StringToHash("Ground");
    private readonly int Speed = Animator.StringToHash("Speed");

    public void Init(PlayerData data)
    {
      _data = data;
    }
    
    private void FixedUpdate()
    {
      if (!IsReady()) 
        return;
      
      UpdateAnimation();
    }

    private void Update()
    {
      if (!IsReady()) 
        return;
      
      Move();
    }
    
    private void Move()
    {
      if (_data.Grounded)
      {
        float move = _data.LockMovement ? 0 : _data.Move;
        _currentAnimator.SetFloat(Speed, Mathf.Abs(move));
      }

      if (_data.Grounded && _data.Jump && _currentAnimator.GetBool(Ground))
      {
        _currentAnimator.SetBool(Ground, false);
      }
    }

    private void UpdateAnimation()
    {
      _currentAnimator.SetBool(Ground, _data.Grounded);
      _currentAnimator.SetFloat(VSpeed, _rigidbody.velocity.y);
    }
    
    private bool IsReady()
    {
      if (_data == null)
      {
        return false;
      }

      return true;
    }
  }
}