﻿using UnityEngine;

namespace Player.StateMachine.State
{
  public class StopState : BaseState
  {
    [SerializeField] private Rigidbody2D _rigidbody;

    public override void Enter()
    {
      base.Enter();
      _rigidbody.velocity = new Vector2(0, 0);
      _data.LockMovement = true;
    }
    
    public override void Tick()
    {
      if (!IsReady)
        return;

      if (CheckIsReadyGoOppositeDirection())
      {
        Flip();
        _data.Wall = false;
        _playerStateMachine.Enter<MovementState>();
      }
    }

    public override void Exit()
    {
      base.Exit();
      _data.LockMovement = false;
    }

    private bool CheckIsReadyGoOppositeDirection()
    {
      if (_data.Move > 0 && !_data.FacingRight)
      {
        return true;
      } 
      
      if (_data.Move < 0 && _data.FacingRight)
      {
        return true;
      }

      return false;
    }
  }
}